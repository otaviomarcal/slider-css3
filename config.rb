require 'bootstrap-sass'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "dev/assets/css"
sass_dir = "dev/source/sass"
images_dir = "dev/assets/img"
javascripts_dir = "dev/assets/js"
fonts_dir = "dev/assets/fonts"

output_style = :compressed
# output_style = :expanded

relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true
color_output = true

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass assets/stylesheets scss && rm -rf sass && mv scss sass

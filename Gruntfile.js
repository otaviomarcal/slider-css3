module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    require('time-grunt')(grunt);

    var PATH = {
        pub: {
            root: './pub',
            js: '<%= PATH.pub.root %>/dev/assets/js/**/*.js',
            img: '<%= PATH.pub.root %>/dev/assets/img'
        },
        dev: {
            root: './dev',
            css: '<%= PATH.dev.root %>/assets/css',
            js: '<%= PATH.dev.root %>/assets/js',
            img: '<%= PATH.dev.root %>/assets/img',
            source: {
                js: '<%= PATH.dev.root %>/source/js',
                libs: '<%= PATH.dev.source.js %>/libs',
                sass: '<%= PATH.dev.root %>/source/sass'
            },
            files: {
                js: {
                    '<%= PATH.dev.js %>/application.js': [
                        '<%= PATH.dev.source.js %>/application/**/*.js',
                        '<%= PATH.dev.source.js %>/utils/**/*.js'
                    ],
                    '<%= PATH.dev.js %>/libs/plugins.js': [
                        '<%= PATH.dev.source.libs %>/**/*.js'
                    ]
                }
            }
        }
    };

    grunt.initConfig({

        PATH: PATH,

        compass: {
            pub: {
                options: {
                    config: 'config.rb'
                }
            }
        },

        uglify: {
            my_target: {
                files: '<%= PATH.dev.files.js %>'
            }
        },

        concat: {
            application_and_libs: {
                files: '<%= PATH.dev.files.js %>'
            }
        },

        htmlmin: {
            pub: {
                options: {
                    collapseWhitespace: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeComments: true
                },

                files: [{
                    expand: true,
                    cwd: '<%= PATH.pub.root %>',
                    src: '{,*/}*.html',
                    dest: '<%= PATH.pub.root %>'
                }]
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '<%= PATH.pub.img %>',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '<%= PATH.pub.img %>'
                }]
            }
        },

        concurrent: {
            dev: ['compass', 'concat'],
            run: ['uglify', 'imagemin', 'htmlmin']
        },

        connect: {
            options: {
                port: 3000,
                open: true,
                livereload: 35729,
                hostname: 'localhost',
                base: '<%= PATH.dev.root %>'
            },

            livereload: {
                options: {
                    middleware: function( connect ) {
                        return [
                            connect.static(PATH.dev.root),
                            connect.static(PATH.dev.css),
                            connect.static(PATH.dev.js),
                            connect.static(PATH.dev.img)
                        ];
                    }
                }
            }
        },

        watch: {
            css: {
                files: '<%= PATH.dev.source.sass %>/**/*.scss',
                tasks: ['compass']
            },

            gruntfile: {
                files: ['Gruntfile.js']
            },

            scripts: {
                files: '<%= PATH.dev.source.js %>/**/*.js',
                tasks: ['concat']
            },

            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },

                files: [
                    '<%= PATH.dev.root %>/{,*/}*.html',
                    '<%= PATH.dev.css %>/{,*/}*.css',
                    '<%= PATH.dev.js %>/{,*/}*.js',
                    '<%= PATH.dev.img %>/{,*/}*'
                ]
            }
        },

        open: {
            dev: {
                path: 'http://localhost:3000',
                app: 'Chrome'
            }
        },

        copy: {
            pub: {
                expand: true,
                src: '<%= PATH.dev.root %>/**',
                dest: '<%= PATH.pub.root %>'
            }
        },

        clean: {
            pub: ['<%= PATH.pub.img %>/tmp/', '<%= PATH.pub.root %>/dev/source/']
        }

    });

    grunt.registerTask('default', ['concurrent:dev', 'connect:livereload', 'watch']);
    grunt.registerTask('run', ['compass', 'copy', 'concurrent:run', 'clean']);

};
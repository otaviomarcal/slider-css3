
function rnotwhite(){
	return (/\S+/g);
}

var selector = function(obj){
    return document.querySelector(obj);
}

var log = function(debug){
	console.log(debug);
}

var isFunction = function (object) {
	if (typeof object != 'function') return false;
	var parent = object.constructor && object.constructor.prototype;
	return parent && hasProperty.call(parent, 'call');
}

//DELEGATE and IS
Element.prototype.is = function(elementSelector) {
    switch(elementSelector[0]) {
        case ".":
            var er = new RegExp(elementSelector.replace(".", ""));
            return this.className.match(er);
            break;
        case "#":
            return this.getAttribute("id") === elementSelector.replace("#", "");
            break;
        default:
            return this.tagName === elementSelector.toUpperCase();
            break;
    }
};
Element.prototype.delegate = function(eventName, elementSelector, cb) {
    var $this = this;
 
    $this.addEventListener(eventName, function (evt) {
        var $this = evt.target;
 
        if ($this.is(elementSelector)) {
            cb($this, evt);
        }
        if ($this.parentNode.is(elementSelector)) {
            cb($this.parentNode, evt);
        }
    });
};

// getElementsByClassName
Element.prototype.getElementsByClassName = function(cl) {
	var retnode = [],
		myclass = new RegExp('\\b'+cl+'\\b'),
		elem = this.getElementsByTagName('*'),
		i = 0;
	
	for (; i < elem.length; i++) {
		var classes = elem[i].className;
		if (myclass.test(classes)) retnode.push(elem[i]);
	}
	
	return retnode;
};

//CSS CLASS MANIPULATION
var addClass = function(objElement, strClass, blnMayAlreadyExist){
	if (objElement.className){
		
		var arrList = objElement.className.split(' ');

		if (blnMayAlreadyExist){
			var strClassUpper = strClass.toUpperCase();

			for ( var i = 0; i < arrList.length; i++ ){
				if ( arrList[i].toUpperCase() == strClassUpper ){
					arrList.splice(i, 1);
					i--;
				}
			}
		}
		arrList[arrList.length] = strClass;
		objElement.className = arrList.join(' ');
	}else{
		objElement.className = strClass;
	}
}

var removeClass = function(objElement, strClass){
	if (objElement.className){
		var arrList 	  = objElement.className.split(' '), 
			strClassUpper = strClass.toUpperCase();

		for (var i = 0; i < arrList.length; i++){
			if ( arrList[i].toUpperCase() == strClassUpper ){
				arrList.splice(i, 1);
				i--;
			}
		}
		objElement.className = arrList.join(' ');
	}
}
//
var isArraylike = function(obj) {
	var length = obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	if ( obj.nodeType === 1 && length ) {
		return true;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}


var isWindow = function( obj ) {
	return obj != null && obj === obj.window;
}